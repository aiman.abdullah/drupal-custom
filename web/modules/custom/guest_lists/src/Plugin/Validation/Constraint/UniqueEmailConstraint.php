<?php

namespace Drupal\guest_lists\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

class UniqueEmailConstraint extends Constraint {

  // // The message that will be shown if the value is not an integer.
  // public $notInteger = '%value is not an integer';

  // The message that will be shown if the value is not unique.
  public $notUnique = '%value is not unique';

}
