<?php

namespace Drupal\guest_lists\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

class CheckUniqueNumber extends FormBase{

  public function getFormId()
  {
    return 'guest_lists_check_unique_number';
  }

  public function buildForm(array $form, FormStateInterface $form_state)
  {
    $form['unique_number'] = [
      '#type' => 'integer',
      '#title' => 'Insert Number',
      '#required' => true,
      '#default_value' => 0
    ];

    return $form;
  }

  public function validateForm(array &$form, FormStateInterface $form_state)
  {

  }

  public function submitForm(array &$form, FormStateInterface $form_state)
  {
    \Drupal::messenger()->addStatus('Number: '.$form_state->getValue('unique_number'));
  }
}
