<?php

namespace Drupal\kanban\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\RedirectResponse;

class KanbanController extends ControllerBase{

  public function index(){

    $tasks = [];
    $groups = [];

    return [
      '#theme' => 'kanban_home',
      '#title' => 'Kanban Board Main Page',
      '#tasks' => $tasks,
      '#groups' => $groups
    ];
  }
}
