<?php

namespace Drupal\event_manager\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Messenger\MessengerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use GuzzleHttp\ClientInterface;

class EventManagerController extends ControllerBase{
  private $table;
  protected $tempStoreFactory;
  protected $messenger;
  protected $clientRequest;

  public function __construct(
    PrivateTempStoreFactory $tempStoreFactory,
    ClientInterface $clientRequest,
    MessengerInterface $messenger
  )
  {
    $this->tempStoreFactory = $tempStoreFactory;
    $this->clientRequest = $clientRequest;
    $this->messenger = $messenger;

    $config = \Drupal::config('event_manager.settings');
    $this->table = $config->get('table');
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('tempstore.private'),
      $container->get('http_client'),
      $container->get('messenger')
    );
  }

  public function index()
  {
    $tempStore = $this->tempStoreFactory->get('multistep_data');
    $params = $tempStore->get('params');
    // Kint($params);

    return [
      '#theme' => 'list_events',
      '#events' => [],
      '#title' => 'All Events'
    ];
  }
}
