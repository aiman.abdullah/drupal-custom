<?php

namespace Drupal\event_manager\Form\Multistep;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Session\SessionManagerInterface;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;

abstract class MultistepFormBase extends FormBase{

  protected $tempStoreFactory;
  protected $sessionManager;
  private $currentUser;
  protected $store;

  public function __construct(
    PrivateTempStoreFactory $temp_store_factory,
    SessionManagerInterface $sessionManager,
    AccountInterface $current_user
  ){
    $this->tempStoreFactory = $temp_store_factory;
    $this->sessionManager = $sessionManager;
    $this->currentUser = $current_user;

    $this->store = $this->tempStoreFactory->get('multistep_data');
  }

  public static function create(ContainerInterface $container){
    return new static(
      $container->get('tempstore.private'),
      $container->get('session_manager'),
      $container->get('current_user')
    );
  }

  public function buildForm(array $form, FormStateInterface $form_state){
    if($this->currentUser->isAnonymous() && !isset($_SESSION['multistep_form_holds_session'])){
      $_SESSION['multistep_form_holds_session'] = true;
      $this->sessionManager->start();
    }

    $form = array();
    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
      '#button_type' => 'primary',
      '#weight' => 10,
    );

    return $form;
  }

  protected function saveData() {
    $this->deleteStore();
    \Drupal::messenger()->addStatus($this->t('The form has been saved.'));
  }

  protected function deleteStore() {
    $keys = ['name', 'email', 'age', 'location'];
    foreach ($keys as $key) {
      $this->store->delete($key);
    }
  }

  public function validateForm(array &$form, FormStateInterface $form_state)
  {

  }

  public function submitForm(array &$form, FormStateInterface $form_state)
  {

  }
}
