<?php

namespace Drupal\event_manager\Form\Multistep;

use Drupal\Core\Form\FormStateInterface;

/**
 * @file
 * EventFormOne
 * hold the form for the event detail.
 */

class EventFormOne extends MultistepFormBase {

  public function getFormId()
  {
    return 'event_manager_event_detail_form';
  }

  public function buildForm(
    array $form,
    FormStateInterface $form_state
  )
  {
    $form = parent::buildForm($form, $form_state);
    $form['event_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Event Name'),
      '#default_value' => $this->store->get('event_name') ? $this->store->get('event_name') : ''
    ];

    $form['event_date'] =[
      '#type' => 'date',
      '#title' => $this->t('Event Date'),
      '#default_value' => $this->store->get('event_date') ? $this->store->get('event_date') : ''
    ];

    $form['expected_person_attending'] = [
      '#type' => 'number',
      '#title' => $this->t('Expected Person Attending'),
      '#default_value' => $this->store->get('expected_person_attending') ? $this->store->get('expected_person_attending') : 0
    ];

    return $form;
  }

  public function submitForm(array &$form, FormStateInterface $form_state)
  {
    $this->store->set('event_name', $form_state->getValue('event_name'));
    $this->store->set('event_date', $form_state->getValue('event_date'));
    $this->store->set('event_time', $form_state->getValue('event_time'));
    $this->store->set('expected_person_attending', $form_state->getValue('expected_person_attending'));
    $form_state->setRedirect('event_manager.event_form_two');
  }
}
