<?php


namespace Drupal\event_manager\Form\Multistep;
use Drupal\Core\Url;

use Drupal\Core\Form\FormStateInterface;
/**
 * @file
 * EventFormTwo
 * hold the form for the event host detail.
 */

class EventFormTwo extends MultistepFormBase {

  public function getFormId()
  {
    return 'event_manager_host_detail_form';
  }

  public function buildForm(array $form, FormStateInterface $form_state)
  {
    $form = parent::buildForm($form, $form_state);

    $form['host_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Host Name'),
      '#default_value' => $this->store->get('host_name') ? $this->store->get('host_name') : ''
    ];

    $form['contact_number'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Contact Number'),
      '#default_value' => $this->store->get('contact_name') ? $this->store->get('contact_name') : ''
    ];

    $form['contact_type'] = [
      '#type' => 'select',
      '#options' => [
        'home' => $this->t('Home'),
        'mobile' => $this->t('Mobile'),
        'office' => $this->t('Office'),
      ],
      '#default_value' => $this->store->get('contact_type') ? $this->store->get('contact_type') : null
    ];

    $form['email'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Email'),
      '#default_value' => $this->store->get('email') ? $this->store->get('email') : ''
    ];

    $form['actions']['previous'] = [
      '#type' => 'link',
      '#title' => $this->t('Previous'),
      '#attributes' => [
        'class' => ['btn', 'btn-default']
      ],
      '#weight' => 0,
      '#url' => Url::fromRoute('event_manager.event_form_two')
    ];

    return $form;
  }

  public function submitForm(array &$form, FormStateInterface $form_state)
  {
    $this->store->set('host_name', $form_state->getValue('host_name'));
    $this->store->set('contact_number', $form_state->getValue('contact_number'));
    $this->store->set('contact_type', $form_state->getValue('contact_type'));
    $this->store->set('email', $form_state->getValue('email'));

    $parent::saveData();
    $form_state->setRedirect('event_manager.index');
  }
}
