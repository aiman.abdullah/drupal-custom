<?php

namespace Drupal\event_manager\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;

class NewEventForm extends FormBase{

  private $table;
  protected $messenger;
  protected $loggerFactory;
  private $tempStoreFactory;

  public function __construct(
    MessengerInterface $messenger,
    LoggerChannelFactoryInterface $loggerFactory,
    PrivateTempStoreFactory $tempStoreFactory
  ) {
    $this->messenger = $messenger;
    $this->loggerFactory = $loggerFactory;
    $this->tempStoreFactory = $tempStoreFactory;
  }

  public static function create(ContainerInterface $container){
    return new static(
      $container->get('messenger'),
      $container->get('logger.factory'),
      $container->get('tempstore.private')
    );
  }

  public function getFormId()
  {
    return 'event_manager_new_event';
  }

  public function buildForm(array $form, FormStateInterface $form_state)
  {
    $form['event_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Event Name'),
      '#default_value' => ''
    ];

    $form['event_date'] =[
      '#type' => 'date',
      '#title' => $this->t('Event Date'),
      '#default_value' => ''
    ];

    $form['expected_person_attending'] = [
      '#type' => 'number',
      '#title' => $this->t('Expected Person Attending'),
      '#default_value' => 0
    ];

    $form['host_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Host Name'),
      '#default_value' => ''
    ];

    $form['contact_number'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Contact Number'),
      '#default_value' => ''
    ];

    $form['contact_type'] = [
      '#type' => 'select',
      '#options' => [
        'home' => $this->t('Home'),
        'mobile' => $this->t('Mobile'),
        'office' => $this->t('Office'),
      ],
      '#default_value' => ''
    ];

    $form['email'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Email'),
      '#default_value' => ''
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => t('submit')
    ];

    return $form;
  }

  public function validateForm(array &$form, FormStateInterface $form_state)
  {

  }

  public function submitForm(array &$form, FormStateInterface $form_state)
  {
    $params['event_name'] = $form_state->getValue('event_name');
    $params['event_date'] = $form_state->getValue('event_date');
    $params['expected_person_attending'] = $form_state->getValue('expected_person_attending');
    $params['host_name'] = $form_state->getValue('host_name');
    $params['contact_number'] = $form_state->getValue('contact_number');
    $params['contact_type'] = $form_state->getValue('contact_type');
    $params['email'] = $form_state->getValue('email');
    $tempstore = $this->tempStoreFactory->get('ex_form_values');

    try {
      $tempstore->set('params', $params);
      $form_state->setRedirect('event_manager.index');
    } catch (\Throwable $th) {
      $this->loggerFactory->get('ex_form_values')->alert(t('@err', ['@err' => $th->getMessage()]));
      $this->messenger->addWarning((t('Unable to save the event details, please proceed again')));
    }
  }
}
